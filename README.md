# KPZ presentation

This repository contain files for KPZ presentation.

Presentation was created using [reveal.js](https://revealjs.com/).

To start presentation, open [index.html](index.html) file in browser.